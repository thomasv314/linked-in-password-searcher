class CreateKeys < ActiveRecord::Migration
  def change
    create_table :keys do |t|
      t.string :pass_hash
      t.string :pass_hash

      t.timestamps
    end
    add_index :keys, :pass_hash
  end
end
