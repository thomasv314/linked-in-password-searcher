# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)
#
Key.create(pass_hash: "000001e4c9b93f3f0682250b6cf8331b7ee68fd8")
Key.create(pass_hash: "00000a1ba31ecd1ae84f75caaa474f3a663f05f4")

puts "Created password and secret"

f = File.open(Rails.root.join("public/SHA1.txt")).each_line do |s|
  k = Key.create(pass_hash: s.slice(0..s.length-2))
  puts "Created #{s}"
end
