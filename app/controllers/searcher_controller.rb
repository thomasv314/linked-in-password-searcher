class SearcherController < ApplicationController

  def get
  end

  def post
    res =  find_hash(params["search"]["password"])
    @uncracked = res[:uncracked]
    @cracked = res[:cracked]
  end

  def find_hash(password)
    sha = Digest::SHA1.hexdigest password
    sha2 = "00000"+sha.slice(5..sha.length)
    k = Key.find_by_pass_hash(sha)
    k2 = Key.find_by_pass_hash(sha2)
    return {:uncracked => k, :cracked => k2} 
  end

end
